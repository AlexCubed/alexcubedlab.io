class LoreUpdatore {
	constructor(input) {
		this.elements = {
			buttons: document.querySelector(".lore_updatore > .buttons"),
			output: document.querySelector(".lore_updatore > .output"),
			preview: document.querySelector(".lore_updatore > .preview")
		};
		this.input = input;
		this.output = "";
		this.settings = {
			default: {
				color: "dark_purple",
				italic: true
			},
			reset: {
				color: "reset",
				// bold: false,
				italic: false,
				// obfuscated: false,
				// strikethrough: false,
				// underlined: false
			}
		};
	}

	set input(value) {
		this._input = value;
		this.update();
	}

	get input() {
		return this._input;
	}

	set output(value) {
		this._output = value;
		this.elements.preview.innerHTML = this.preview;
		this.elements.output.innerHTML = this.output;
		if (this.output === "") this.elements.buttons.classList.add("hidden");
		else this.elements.buttons.classList.remove("hidden");
	}

	get output() {
		return this._output;
	}

	update() {
		try {
			let input = JSON.parse(this.input);
			let output = [];
			for (let s of input) {
				let out = "";
				while (s.length > 0) {
					if (s[0] === "§") {
						let c = this.parse_character(s[1]);
						if (this.type(out) === "string") {
							if (out.length > 0) {
								if (c.type === "color") out = [out, { ...this.settings.reset, text: "", color: c.value }];
								if (c.type === "format") out = [out, { text: "", [c.value]: true }];
								if (c.type === "reset") out = [out, { ...this.settings.reset, text: ""}];
							} else {
								if (c.type === "color") out = { ...this.settings.reset, text: out, color: c.value };
								if (c.type === "format") out = { text: out, [c.value]: true };
								if (c.type === "reset") out = { ...this.settings.reset, text: out };
							};
						} else if (this.type(out) === "object") {
							if (c.type === "color") out = ["", out, { ...this.settings.reset, text: "", color: c.value }];
							if (c.type === "format") {
								if (out.text.length > 0) out = ["", out, { ...out, text: "", [c.value]: true }];
								else out[c.value] = true;
							};
							if (c.type === "reset") out = ["", out, { ...this.settings.reset, text: "" }];
						} else if (this.type(out) === "array") {
							if (c.type === "color") out.push({ ...this.settings.reset, text: "", color: c.value });
							if (c.type === "format") {
								if (this.type(out[out.length - 1]) === "string") {
									out.push({ text: "", [c.value]: true });
								} else if (this.type(out[out.length - 1]) === "object") {
									if (out[out.length - 1].text.length > 0) out.push({ ...out[out.length - 1], text: "", [c.value]: true });
									else out[out.length - 1][c.value] = true;
								};
							};
							if (c.type === "reset") out.push({ ...this.settings.reset, text: "" });
						};
						s = s.substring(1);
					} else if (s[0] !== "§") {
						if (this.type(out) === "string") out += s[0];
						else if (this.type(out) === "object") out.text += s[0];
						else if (this.type(out) === "array") {
							if (this.type(out[out.length - 1]) === "string") out[out.length - 1] += s[0];
							if (this.type(out[out.length - 1]) === "object") out[out.length - 1].text += s[0];
						};
					};
					s = s.substring(1);
				};
				function same_display(obj1, obj2) {
					let success = true;
					if (obj1.color !== obj2.color) success = false;
					if (obj1.bold !== obj2.bold) success = false;
					if (obj1.italic !== obj2.italic) success = false;
					if (obj1.obfuscated !== obj2.obfuscated) success = false;
					if (obj1.strikethrough !== obj2.strikethrough) success = false;
					if (obj1.underlined !== obj2.underlined) success = false;
					return success;
				};
				let good;
				if (this.type(out) == "string") good = out;
				if (this.type(out) === "object") good = out.text.length > 0 ? out : "";
				if (this.type(out) === "array") {
					good = [];
					for (let i = 0; i < out.length; i++) {
						if (this.type(out[i]) === "string") good.push(out[i]);
						if (this.type(out[i]) === "object") {
							if (out[i].text.length > 0) good.push(out[i]);
						}
					};
					if (good.length >= 2) {
						let i = 0;
						while (i < good.length - 1) {
							if (this.type(good[i]) === "object" && this.type(good[i + 1]) === "object" && same_display(good[i], good[i + 1])) {
								good[i].text += good[i + 1].text;
								good.splice(i + 1, 1);
								i--;
							}
							i++;
						};
					};
					if (good.length <= 2 && good[0].length === 0) good = good[good.length - 1];
				};
				if (typeof good !== "undefined") output.push(good);
			};
			this.output = JSON.stringify(output.map(a => JSON.stringify(a)));
		} catch (error) {
			this.output = "";
		}
	}

	get preview() {
		try {
			let input = JSON.parse(this.output);
			input = input.map(stringified_line => JSON.parse(stringified_line));
			input = input.map(line => {
				if (this.type(line) === "string") return {...this.settings.default, text: line};
				if (this.type(line) === "object") return {...this.settings.default, ...line};
				if (this.type(line) === "array") {
					return line.map(comp => {
						if (this.type(comp) === "string") return {...this.settings.default, text: comp};
						if (this.type(comp) === "object") return {...this.settings.default, ...comp};
					});
				};
			});
			let lines = [];
			for (let line of input) {
				let new_line = "";
				if (this.type(line) === "string") {
					new_line += line;
				};
				if (this.type(line) === "object") {
					let formats = [];
					if (line.bold) formats.push("bold");
					if (line.italic) formats.push("italic");
					if (line.obfuscated) formats.push("obfuscated");
					if (line.strikethrough) formats.push("strikethrough");
					if (line.underlined) formats.push("underlined");
					new_line += `<span class="${line.color} ${formats.join(" ")}">${line.text}</span>`;
				};
				if (this.type(line) === "array") {
					for (let component of line) {
						if (this.type(component) === "string") {
							new_line += component;
						};
						if (this.type(component) === "object") {
							let formats = [];
							if (component.bold) formats.push("bold");
							if (component.italic) formats.push("italic");
							if (component.obfuscated) formats.push("obfuscated");
							if (component.strikethrough) formats.push("strikethrough");
							if (component.underlined) formats.push("underlined");
							new_line += `<span class="${component.color} ${formats.join(" ")}">${component.text}</span>`;
						};
					};
				};
				lines.push(new_line);
			};
			return lines.join("<br>") + "<br>";
		} catch (error) {
			return "";
		}
	}

	type(value) {
		if (typeof value === "string") return "string";
		if (typeof value === "object") {
			if (Array.isArray(value)) return "array";
			else return "object";
		}
	}

	parse_character(char) {
		if (char === "0") return { type: "color", value: "black" };
		if (char === "1") return { type: "color", value: "dark_blue" };
		if (char === "2") return { type: "color", value: "dark_green" };
		if (char === "3") return { type: "color", value: "dark_aqua" };
		if (char === "4") return { type: "color", value: "dark_red" };
		if (char === "5") return { type: "color", value: "dark_purple" };
		if (char === "6") return { type: "color", value: "gold" };
		if (char === "7") return { type: "color", value: "gray" };
		if (char === "8") return { type: "color", value: "dark_gray" };
		if (char === "9") return { type: "color", value: "blue" };
		if (char === "a") return { type: "color", value: "green" };
		if (char === "b") return { type: "color", value: "aqua" };
		if (char === "c") return { type: "color", value: "red" };
		if (char === "d") return { type: "color", value: "light_purple" };
		if (char === "e") return { type: "color", value: "yellow" };
		if (char === "f") return { type: "color", value: "white" };
		if (char === "k") return { type: "format", value: "obfuscated" };
		if (char === "l") return { type: "format", value: "bold" };
		if (char === "m") return { type: "format", value: "strikethrough" };
		if (char === "n") return { type: "format", value: "underlined" };
		if (char === "o") return { type: "format", value: "italic" };
		if (char === "r") return { type: "reset", value: "reset" };
	}
}